1. This folder contains the hotels dataset published with the article

Wajahat hussain, Javier Civera, Luis Montano, Martial Hebert, "Dealing with small data and training blind spots in the Manhattan world", WACV 2016.

2. Dataset characteristics:
   hotels: 68, The complete hotel name list is given in hotels.txt file
   train images: 1360 (20 images per hotel), The complete list is given in train.txt file
   test images : 680  (10 images per hotel), The complete list is given in test.txt file
   
3. Source: 
   http://www.tripadvisor.com/

4. Contact:

For any questions, I can be reached via email at wajih.1234@gmail.com
  
